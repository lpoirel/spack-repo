##############################################################################
# Copyright (c) 2017, Inria
# Produced at Inria.
#
# This file is part of https://gitlab.inria.fr/solverstack/spack-repo
#
# For details, see https://github.com/spack/spack
#
##############################################################################
#
from spack import *

class Parsec(CMakePackage):
    """Parallel Runtime Scheduling and Execution Controller."""
    homepage = "https://bitbucket.org/mfaverge/parsec"

    version ('mfaverge', git='https://bitbucket.org/mfaverge/parsec.git', branch='mymaster')

    variant('shared', default=True, description='Enable shared library')

    depends_on("cmake")
    depends_on("hwloc@:1.999")
    depends_on("mpi")

    def cmake_args(self):

        spec = self.spec

        if spec.satisfies('+shared'):
            args = ["-DBUILD_SHARED_LIBS=ON"]
        else:
            args = ["-DBUILD_SHARED_LIBS=OFF"]

        return args
