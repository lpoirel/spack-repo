##############################################################################
from spack import *
import platform


class PyMumps(PythonPackage):
    """Python wrapper for mumps"""

    homepage = "https://gitlab.inria.fr/solverstack/genfem"
    url = "https://gitlab.inria.fr/solverstack/genfem.git"

    version('git',  git='https://gitlab.inria.fr/gmarait/PYMUMPS.git')

    extends('python', ignore=r'bin/pytest')

    depends_on('mumps@5.0.2')
    depends_on('py-scipy')
    depends_on('py-mpi4py')
    depends_on('py-setuptools')

    def install(self, spec, prefix):
        python_version = spec['python'].version.up_to(2)
        arch = '{0}-{1}'.format(platform.system().lower(), platform.machine())

        PythonPackage.install(self, spec, prefix)

        deps = ["blas","lapack", "mpi", "scalapack",
                "metis", "parmetis",
                "scotch", "ptscotch",
                "mumps"]
        list_libs = []
        for dep in deps:
            if dep in spec:
                # No deps property in mumps/package.py
                if dep=="mumps":
                    dep_libs = ["pord", "mumps_common"]
                    if "+float" in spec["mumps"]:
                        dep_libs += ["smumps"]
                        if "+complex" in spec["mumps"]:
                            dep_libs += ["cmumps"]
                    if "+double" in spec["mumps"]:
                        dep_libs += ["dmumps"]
                        if "+complex" in spec["mumps"]:
                            dep_libs += ["zmumps"]
                    dep_libs = ["{}/lib/lib{}.so".format(
                        spec["mumps"].prefix, dep)
                                for dep in dep_libs]
                else:
                    dep_libs = spec[dep].libs
                list_libs.extend(dep_libs)

        my_prefix = join_path(
            prefix.lib,
            'python{0}'.format(python_version),
            'site-packages',
            'pymumps')

        with working_dir(my_prefix):
            with open("lib_cache", "w") as f:
                f.write('\n'.join(list_libs))

