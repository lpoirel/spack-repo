##############################################################################
# Copyright (c) 2017, Inria
# Produced at Inria.
#
# This file is part of https://gitlab.inria.fr/solverstack/spack-repo
#
# For details, see https://github.com/spack/spack
#
##############################################################################
#
from spack import *
import os
import sys

def get_submodules():
    if os.path.exists(".git"):
        git = which('git')
        git('submodule', 'update', '--init', '--recursive')

class Chameleon(CMakePackage):
    """Dense Linear Algebra for Scalable Multi-core Architectures and GPGPUs"""
    homepage = "https://gitlab.inria.fr/solverstack/chameleon"

    version('master', git='https://gitlab.inria.fr/solverstack/chameleon.git', submodules=True)

    variant('shared', default=True, description='Build chameleon as a shared library')
    variant('mpi', default=True, description='Enable MPI')
    variant('cuda', default=False, description='Enable CUDA')
    variant('fxt', default=False, description='Enable FxT tracing support through StarPU')
    variant('simgrid', default=False, description='Enable simulation mode through StarPU+SimGrid')
    variant('starpu', default=True, description='Use StarPU runtime')
    variant('quark', default=False, description='Use Quark runtime instead of StarPU')
    variant('examples', default=True, description='Enable compilation and installation of example executables')

    depends_on("cmake")
    depends_on("blas", when='~simgrid')
    depends_on("lapack", when='~simgrid')
    depends_on("starpu", when='+starpu')
    depends_on("starpu~mpi", when='+starpu~mpi')
    depends_on("starpu+cuda", when='+starpu+cuda~simgrid')
    depends_on("starpu+fxt", when='+starpu+fxt')
    depends_on("starpu+simgrid", when='+starpu+simgrid')
    depends_on("starpu+mpi~shared+simgrid", when='+starpu+simgrid+mpi')
    depends_on("quark", when='+quark')
    depends_on("mpi", when='+mpi~simgrid')
    depends_on("cuda", when='+cuda~simgrid')
    depends_on("fxt", when='+fxt+starpu')

    @run_before('cmake')
    def update_submodules(self):

        spec = self.spec

        if spec.satisfies('@master'):
            if not os.path.exists("cmake_modules/morse_cmake/modules"):
                get_submodules()


    def cmake_args(self):

        spec = self.spec
        args = std_cmake_args

        args.extend([
            "-Wno-dev",
            "-DCMAKE_COLOR_MAKEFILE:BOOL=ON",
            "-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON"])

        if spec.satisfies('+shared'):
            # Enable build shared libs.
            args.extend(["-DBUILD_SHARED_LIBS=ON"])

        if spec.satisfies('+examples'):
            # Enable Examples here.
            args.extend(["-DCHAMELEON_ENABLE_EXAMPLE=ON"])
            args.extend(["-DCHAMELEON_ENABLE_TESTING=ON"])
            args.extend(["-DCHAMELEON_ENABLE_TIMING=ON"])
        else:
            # Enable Examples here.
            args.extend(["-DCHAMELEON_ENABLE_EXAMPLE=OFF"])
            args.extend(["-DCHAMELEON_ENABLE_TESTING=OFF"])
            args.extend(["-DCHAMELEON_ENABLE_TIMING=OFF"])
        if spec.satisfies('+debug'):
            # Enable Debug here.
            args.extend(["-DCMAKE_BUILD_TYPE=Debug"])
        if spec.satisfies('+mpi'):
            # Enable MPI here.
            args.extend(["-DCHAMELEON_USE_MPI=ON"])
            args.extend(["-DMPI_C_COMPILER=%s" % self.spec['mpi'].mpicc])
            args.extend(["-DMPI_CXX_COMPILER=%s" % self.spec['mpi'].mpicxx])
            args.extend(["-DMPI_Fortran_COMPILER=%s" % self.spec['mpi'].mpifc])
        if spec.satisfies('+cuda'):
            # Enable CUDA here.
            args.extend(["-DCHAMELEON_USE_CUDA=ON"])
        else:
            args.extend(["-DCHAMELEON_USE_CUDA=OFF"])
        if spec.satisfies('+fxt'):
            # Enable FxT here.
            args.extend(["-DCHAMELEON_ENABLE_TRACING=ON"])
        if spec.satisfies('+simgrid'):
            # Enable SimGrid here.
            args.extend(["-DCHAMELEON_SIMULATION=ON"])
        if spec.satisfies('+quark') and spec.satisfies('+starpu'):
            raise InstallError('variant +quark and +starpu are mutually exclusive, please choose one.')
        if spec.satisfies('~quark') and spec.satisfies('~starpu'):
            raise InstallError('Chameleon requires a runtime system to be enabled, either +quark or +starpu, please choose one.')
        if spec.satisfies('+quark'):
            # Enable Quark here.
            args.extend(["-DCHAMELEON_SCHED_QUARK=ON"])
        else:
            args.extend(["-DCHAMELEON_SCHED_QUARK=OFF"])
        if spec.satisfies('+starpu'):
            # Enable StarPU here.
            starpu = self.spec['starpu']
            args.extend(["-DCHAMELEON_SCHED_STARPU=ON"])
        else:
            args.extend(["-DCHAMELEON_SCHED_STARPU=OFF"])

        if spec.satisfies('~simgrid'):
            if '^atlas' in spec:
                raise InstallError('Chameleon requires to have a full BLAS LAPACK implementation, with CBLAS, LAPACKE and TMGLIB. ATLAS cannot be used.')
                #args.extend(["-DBLA_VENDOR=ATLAS"])
            elif '^intel-mkl' in spec or '^intel-parallel-studio+mkl' in spec:
                if '^intel-mkl threads=none' in spec or '^intel-parallel-studio threads=none' in spec:
                    args.extend(["-DBLA_VENDOR=Intel10_64lp_seq"])
                else:
                    args.extend(["-DBLA_VENDOR=Intel10_64lp"])
            elif '^netlib-lapack' in spec:
                args.extend(["-DBLA_VENDOR=Generic"])
            elif '^openblas' in spec:
                args.extend(["-DBLA_VENDOR=Open"])
            elif '^veclibfort' in spec:
                raise InstallError('Chameleon with veclibfort has never been tested. Please ')
                #args.extend(["-DBLA_VENDOR=Apple"])

        return args
